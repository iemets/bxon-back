<?php

    /*
    |--------------------------------------------------------------------------
    | Предпросмотр данных
    |--------------------------------------------------------------------------
    */

    function pre($data, $die = true)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        if ($die) die();
    }

    /*
    |--------------------------------------------------------------------------
    | Генератор паролей
    |--------------------------------------------------------------------------
    */

    function _generatePassword($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

?>
