<?php

function set_content_type_json(){
    header('Content-type: application/json');
}

function get($param = null){
    $ci =& get_instance();
    return $ci->input->get($param, true);
}

function post($param = null){
    $ci =& get_instance();
    return $ci->input->post($param, true);
}

function json($param = null){
    $ci =& get_instance();
    return $ci->input->json($param);
}

function translate($key){
    return $key;
}
