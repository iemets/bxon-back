<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Payment extends Eloquent {

    const CURRENCY_RUR = 'rur';
    const CURRENCY_USD = 'usd';
    const CURRENCY_EUR = 'eur';

    const CURRENCIES = [
        self::CURRENCY_RUR,
        self::CURRENCY_USD,
        self::CURRENCY_EUR
    ];

    const TYPE_HOURLY = 'hourly';
    const TYPE_DAILY = 'daily';
    const TYPE_MONTHLY = 'monthly';

    const TYPES = [
        self::TYPE_HOURLY,
        self::TYPE_DAILY,
        self::TYPE_MONTHLY
    ];

    const WALLET_CARD = 'card';
    const WALLET_YANDEX = 'yandex';
    const WALLET_KIWI = 'kiwi';

    const WALLETS = [
        self::WALLET_CARD,
        self::WALLET_YANDEX,
        self::WALLET_KIWI
    ];

    protected $table = 'payment';

    public $timestamps = true;
    public $primaryKey = 'id';
    protected $fillable = [
        'rate',
        'type',
        'currency',
        'description'
    ];

    function user(){
        return $this->belongsTo('User');
    }
}