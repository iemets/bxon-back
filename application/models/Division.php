<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Division extends Eloquent{
    protected $table = 'division';

    protected $fillable = ['name'];

    public $timestamps = true;
}