<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Ownership_Organization extends Eloquent {

    protected $table = 'ownership_organization';

    public $timestamps = false;

    protected $fillable = [
        'ownership_id',
        'full_name',
        'short_name',
        'OGRN',
        'legal_address',
        'postal_address',
        'physical_address',
        'OKATO',
        'OPF',
        'OKVED',
        'director_full_name',
        'accountant_full_name',
        'phone',
        'email',
        'site'
    ];
}
