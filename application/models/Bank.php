<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Bank extends Eloquent {

    protected $table = 'bank';

    protected $fillable = [
        'user_id',
        'name',
        'BIK',
        'account',
        'correspondent_account'
    ];

}
