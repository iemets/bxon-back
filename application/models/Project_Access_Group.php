<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Project_Access_Group extends Eloquent {


    const ENTITY_PROJECT_ACCESS_GROUP = 'project_access_group';

    protected $table = 'project_access_group';
    public $guarded = array();
    public $timestamps = false;


    public function users() {
        return $this->morphToMany('User', 'entity', 'user_entity');
    }

    public function access() {
        return $this->hasMany('Project_Access', 'access_group_id');
    }

}
