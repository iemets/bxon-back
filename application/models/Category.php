<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Category extends Eloquent {

    const PARENT_NONE = 'none';
    const PARENT_PROJECT = 'project';
    const PARENT_TASK = 'task';
    const PARENT_BUG = 'bug';
    const PARENT_TRAINING = 'training';
    const PARENT_DEVELOPER = 'developer';
    const PARENT_USER = 'user';

    protected $table = 'category';

    public $hidden = array('pivot');
    public $timestamps = false;
    protected $fillable = [
        'parent_id',
        'name',
        'description'
    ];

    function users(){
        return $this->morphedByMany('User', 'entity', 'categories_entities');
    }

    function projects(){
        return $this->morphedByMany('Projects', 'entity', 'categories_entities');
    }

    function tasks(){
        return $this->morphedByMany('Task', 'entity', 'categories_entities');
    }

    function parent(){
        return $this->hasOne('Category', 'parent_id');
    }

    function children(){
        return $this->hasMany('Category', 'id', 'parent_id');
    }





}
