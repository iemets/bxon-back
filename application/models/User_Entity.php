<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class User_Entity extends Eloquent {

    protected $table = 'user_entity';

    const ENTITY_TYPE_NONE = 'none';
    const ENTITY_TYPE_PROJECT = 'project';
    const ENTITY_TYPE_TASK = 'task';
    const ENTITY_TYPE_CATEGORY = 'category';
    const ENTITY_TYPE_TAG = 'tag';

    const ENTITY_TYPES = [
        'ENTITY_TYPE_NONE' => self::ENTITY_TYPE_NONE,
        'ENTITY_TYPE_PROJECT' => self::ENTITY_TYPE_PROJECT,
        'ENTITY_TYPE_TASK' => self::ENTITY_TYPE_TASK,
        'ENTITY_TYPE_CATEGORY' => self::ENTITY_TYPE_CATEGORY,
        'ENTITY_TYPE_TAG' => self::ENTITY_TYPE_TAG,
    ];

    protected $fillable = [
        'user_id',
        'entity_type',
        'entity_id',
    ];

    public $timestamps = false;
}
