<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Schedule extends Eloquent {

    protected $table = 'schedule';

    const WEEK_DAY_MONDAY = 'sunday';
    const WEEK_DAY_TUESDAY = 'tuesday';
    const WEEK_DAY_WEDNESDAY = 'wednesday';
    const WEEK_DAY_THURSDAY = 'thursday';
    const WEEK_DAY_FRIDAY = 'friday';
    const WEEK_DAY_SATURDAY = 'saturday';
    const WEEK_DAY_SUNDAY = 'sunday';

    const WEEK_DAYS = [
       'WEEK_DAY_MONDAY' => self::WEEK_DAY_MONDAY,
       'WEEK_DAY_TUESDAY' => self::WEEK_DAY_TUESDAY,
       'WEEK_DAY_WEDNESDAY' => self::WEEK_DAY_WEDNESDAY,
       'WEEK_DAY_THURSDAY' => self::WEEK_DAY_THURSDAY,
       'WEEK_DAY_FRIDAY' => self::WEEK_DAY_FRIDAY,
       'WEEK_DAY_SATURDAY' => self::WEEK_DAY_SATURDAY,
       'WEEK_DAY_SUNDAY' => self::WEEK_DAY_SUNDAY
    ];

    public $timestamps = false;
    protected $fillable = ['user_id', 'week_day', 'time_from', 'time_to'];

    function user(){
        return $this->hasOne('User');
        //return $this->belongsTo('User');
    }


}