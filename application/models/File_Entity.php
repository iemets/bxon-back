<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class File_Entity extends Eloquent {

    protected $table = 'file_entity';

    const ENTITY_TYPE_NONE = 'none';
    const ENTITY_TYPE_PROJECT = 'project';
    const ENTITY_TYPE_TASK = 'task';
    const ENTITY_TYPE_BUG = 'bug';
    const ENTITY_TYPE_TRAINING = 'training';
    const ENTITY_TYPE_DEVELOPER = 'developer';
    const ENTITY_TYPE_USER = 'user';
    const ENTITY_TYPE_RESUME = 'resume';
    const ENTITY_TYPE_PORTFOLIO = 'portfolio';
    const ENTITY_TYPE_SECURITY = 'security';
    const ENTITY_TYPE_PROJECT_ACCESS = 'project_access';

    const ENTITY_TYPES = [
        'ENTITY_TYPE_NONE' => self::ENTITY_TYPE_NONE,
        'ENTITY_TYPE_PROJECT' => self::ENTITY_TYPE_PROJECT,
        'ENTITY_TYPE_TASK' => self::ENTITY_TYPE_TASK,
        'ENTITY_TYPE_BUG' => self::ENTITY_TYPE_BUG,
        'ENTITY_TYPE_TRAINING' => self::ENTITY_TYPE_TRAINING,
        'ENTITY_TYPE_DEVELOPER' => self::ENTITY_TYPE_DEVELOPER,
        'ENTITY_TYPE_USER' => self::ENTITY_TYPE_USER,
        'ENTITY_TYPE_RESUME'  => self::ENTITY_TYPE_RESUME,
        'ENTITY_TYPE_PORTFOLIO' => self::ENTITY_TYPE_PORTFOLIO,
        'ENTITY_TYPE_SECURITY' => self::ENTITY_TYPE_SECURITY,
        'ENTITY_TYPE_PROJECT_ACCESS' => self::ENTITY_TYPE_PROJECT_ACCESS
    ];

    protected $fillable = [
        'file_id',
        'entity_type',
        'entity_id',
    ];

    public $primaryKey = 'file_id';
    public $timestamps = false;
}
