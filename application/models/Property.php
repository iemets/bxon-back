<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Property extends Eloquent {

    protected $table = 'property';

    protected $fillable = ['user_id', 'type', 'value'];

    public $timestamps = true;

    const PROPERTY_SKYPE = 'skype';
    const PROPERTY_EMAIL = 'email';
    const PROPERTY_PHONE = 'phone';

    const PROPERTY_TYPES = [
        'PROPERTY_SKYPE' => self::PROPERTY_SKYPE,
        'PROPERTY_EMAIL' => self::PROPERTY_EMAIL,
        'PROPERTY_PHONE' => self::PROPERTY_PHONE
    ];

    public function user() {
        return $this->belongsTo('User', 'user_id', 'id');
    }
}
