<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Ownership_Person extends Eloquent {

    protected $table = 'ownership_person';

    public $timestamps = false;

    protected $fillable = [
        'ownership_id',
        'first_name',
        'middle_name',
        'last_name',
        'address',
        'INN',
        'site',
        'email',
        'phone',
        'contacts'
    ];

}
