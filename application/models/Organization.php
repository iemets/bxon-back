<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Organization extends Eloquent {

    public $timestamps = true;

    protected $table = 'organization';


}