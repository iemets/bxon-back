<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Tag extends Eloquent {

    const RANK_NONE = 'none';
    const RANK_NEGATIVE = 'negative';
    const RANK_NEUTRAL = 'neutral';
    const RANK_POSITIVE = 'positive';

    const RANKS = [
        self::RANK_NONE,
        self::RANK_NEGATIVE,
        self::RANK_NEUTRAL,
        self::RANK_POSITIVE
    ];

    const TYPE_CHAR = 'char';
    const TYPE_SPEC = 'spec';

    const TYPES = [
        self::TYPE_CHAR,
        self::TYPE_SPEC
    ];

    protected $table = 'tag';

    public $timestamps = true;
    public $primaryKey = 'id';
    protected $fillable = ['type', 'rank', 'description'];

    function users(){
        return $this->morphToMany('User', 'entity', 'user_entity');
    }
}