<?php
class Migration extends MX_Controller{

    function __construct(){
        parent::__construct();
        $this->load->library('migration');
    }

    function index($migration = ''){
        $migrations = $this->migration->find_migrations();

        foreach($migrations as $migration){
            $name = str_replace('.php', '', mb_substr(basename($migration), 11));

            //echo '<pre>'; print_r($name); echo '</pre>';
        }
        $this->migration->version(0);
        $this->migration->latest();
    }

    function order(){
        $migrations = $this->migration->find_migrations();
        $number = 1;
        foreach($migrations as $migration){
            if($number < 10){
                $str_number = '00' . $number;
            }elseif($number < 100){
                $str_number = '0' . $number;
            }else{
                $str_number = $number;
            }
            $path = pathinfo($migration);
            $path = $path['dirname'] . DIRECTORY_SEPARATOR;

            $chars = mb_substr(basename($migration), 0, 3);
            $name = str_replace($chars, $str_number, basename($migration));

            $number++;

            //echo '<pre>'; print_r($path); echo '</pre>';
            rename($migration, $path . $name);
        }
        echo '<pre>'; print_r($migrations); echo '</pre>';
    }

    function reorder(){
        $ds = DIRECTORY_SEPARATOR;
        $path = APPPATH . $ds . 'migrations' . $ds;
        $migrations = scandir($path);
        $number = 1;
        foreach($migrations as $migration){
            if($migration == '.' || $migration == '..') continue;

            $migration = $path . $migration;

            if($number < 10){
                $str_number = '00' . $number;
            }elseif($number < 100){
                $str_number = '0' . $number;
            }else{
                $str_number = $number;
            }
            $path = pathinfo($migration);
            $path = $path['dirname'] . DIRECTORY_SEPARATOR;

            $chars = mb_substr(basename($migration), 0, 3);
            $name = str_replace($chars, $str_number, basename($migration));

            $number++;

            //echo '<pre>'; print_r($path); echo '</pre>';
            rename($migration, $path . $name);
        }
    }




}