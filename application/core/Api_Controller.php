<?php
class Api_Controller extends MX_Controller{

    public $im;

    public $errors = [];

    protected $parent = null;

    function __construct(){
        parent::__construct();
        return;
        /*
        $this->load->module('auth');
        $this->load->module('core/user/user_core', null, 'user');

        $data = $this->auth->_checkToken();

        if ( ! $data) {
            $this->output->send(['error' => 'auth.false'], 401);
        }

        $im = $this->user->_getById($data->user_id);

        if( ! $im){
            $this->output->send(['error' => 'auth.false'], 401);
        }

        $this->im = $im;
        */
    }


    /**
     * @author jonston
     * @param mixed $name error name
     * @param mixed $errors error value
     */
    function setErrors($errors){
        parse_str(http_build_query($errors), $result);
        return $result;
    }



}