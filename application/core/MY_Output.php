<?php
class MY_Output extends CI_Output{

    function __construct(){
        parent::__construct();
    }

    function send($output, $status = 200, $type = 'json'){
        $this->set_status_header($status);

        $output = translate($output);

        if($type === 'json'){
            header('Content-Type: application/json');
            $output = json_encode($output);
        }

        exit($output);
    }

}