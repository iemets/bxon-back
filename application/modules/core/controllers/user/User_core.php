<?php
class User_core extends Core_Controller{

    protected $roles;

    function __construct(){
        parent::__construct();

        $this->load->module('core/category/category_module', null, 'category');

        $this->roles = [
            Role::ROLE_ADMIN,
            Role::ROLE_PM,
            Role::ROLE_CUSTOMER,
            Role::ROLE_DEVELOPER,
            Role::ROLE_OBSERVER,
            Role::ROLE_BOT,
        ];

        $this->create_user_rules = array(
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'role',
                'label' => 'role',
                'rules' => 'trim|required|in_list['.implode(',', $this->roles).']'
            ),
            array(
                'field' => 'first_name',
                'label' => 'first_name',
                'rules' => 'required'
            ),
            array(
                'field' => 'middle_name',
                'label' => 'middle_name',
                'rules' => 'required'
            ),
            array(
                'field' => 'last_name',
                'label' => 'last_name',
                'rules' => 'required'
            )
        );

        $this->save_resume_rules = array(
            array(
                'field' => 'user_id',
                'label' => 'user_id',
                'rules' => 'required|integer|user_exist'
            ),
            array(
                'field' => 'sites[]',
                'label' => 'Sites',
                'rules' => 'required'
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'required'
            )
        );
    }

    function index(){
        return $this->_getList();
    }

    function _getList($filters = null, $sort_by = null, $sort_order = null, $offset = null, $limit = 10){

        if( ! in_array($sort_by, ['id', 'role', 'email'])){
            $sort_by = 'id';
        }

        if( ! in_array(mb_strtolower($sort_order), ['asc', 'desc'])){
            $sort_order = 'asc';
        }

        $limit = (int) $limit;

        if($limit < 1 || $limit > 100){
            $limit = 10;
        }

        $offset = (int) $offset;

        $users = new User();

        // Фильтр по роли
        if(isset($filters['role']) && in_array($filters['role'], $this->roles)){
            $users = $users->where(
                'role', '=', $filters['role']
            );
        }

        if( ! empty($filters['email'])){
            $users = $users->where(
                'email', 'like', $filters['email'].'%'
            );
        }

        $users = $users->with('categories');

        // Фильтр по категории
        if( ! empty($filters['category'])){
            $users = $users->whereHas('categories', function($query) use ($filters){
                $query->where([
                    'id' => $filters['category']
                ]);
            });
        }

        if( ! empty($filters['label'])){
            $users = $users->where(
                'label', 'like', $filters['label'].'%'
            );
        }

        $users = $users->with('properties');

        $total = $users->count();
        $users = $users->skip($offset)->take($limit)->orderBy($sort_by, $sort_order)->get();

        $count = $users->count();

        if ( ! $count) return null;

        $users['pagination'] = array(
            "limit" => $limit,
            "total" => $total
        );

        return $users->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Получит пользователя по ID
    |--------------------------------------------------------------------------
    */

    function _getById($id){
        $user = User::where('id', $id);

        if( ! $user->count()){
            return null;
        }

        return $user->with('properties', 'categories')->first()->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Добавить пользователя
    |--------------------------------------------------------------------------
    */

    function _create($data){

        $validation = $this->form_validation;

        $validation->set_data($data);
        $validation->set_rules([
            array(
                'field' => 'first_name',
                'label' => 'first_name',
                'rules' => 'required'
            ),
            array(
                'field' => 'middle_name',
                'label' => 'middle_name',
                'rules' => 'required'
            ),
            array(
                'field' => 'last_name',
                'label' => 'last_name',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'role',
                'label' => 'role',
                'rules' => 'trim|required|in_list['.implode(',', $this->roles).']'
            )
        ]);

        if( ! $validation->run()){
            $this->setErrors('user', $validation->error_array());
            return false;
        }

        $password = _generatePassword(5);
        $data['password'] = md5($password);

        $user = new User();
        $user->fill($data);

        if($user->save()){
            return ['user_id' => $user->id];
        }else{
            return false;
        }

    }

    /*
    |--------------------------------------------------------------------------
    | Редактировать пользователя
    |--------------------------------------------------------------------------
    */

    function _update($user_id, $data){

        $user = User::find($user_id);

        if( ! $user){
            $this->errors = ['user_id' => 'user_not_found'];
            return false;
        }

        $validation = $this->form_validation;

        $validation->set_data($data);
        $validation->set_rules($this->create_user_rules);

        if( ! $validation->run()){
            $this->errors = $validation->error_array();
            return false;
        }

        $user->fill($data);

        if($user->save()){
            return ['user_id' => $user->id];
        }else{
            return false;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Удалить пользователя
    |--------------------------------------------------------------------------
    */

    function _delete($user_id){
        $user = User::find($user_id);

        if( ! $user){
            return false;
        }

        if($user->delete()){
            return ['user_id' => $user->id];
        }else{
            return false;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Добавить свойства пользователю
    |--------------------------------------------------------------------------
    */

    function _addProperties($user_id, $props = []){

        if (empty($props)) return false;

        foreach ($props as $prop) {
            $user_property = new User_Property();
            $user_property->fill($prop);
            $user_property->user_id = $user_id;
            $user_property->save();
        }

        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Удалить свойства юзера
    |--------------------------------------------------------------------------
    */

    function _removeProperties($user_id){
        $remove = User_Property::where('user_id',intval($user_id))
            ->delete();

        return ($remove) ? true:false;
    }


    /*
    |--------------------------------------------------------------------------
    | Проверка: свободен ли email
    |--------------------------------------------------------------------------
    */

    private function is_email_available($email, $user_id = 0) {

        $user = User::where('email', strtolower($email))->get()->first();

        if (!$user){
            return true;
        }
        else{
            if ($user_id > 0 && $user_id == $user->id)
                return true;
            else return false;
        }

    }

    /*
    |--------------------------------------------------------------------------
    | Получение данных резюме
    |--------------------------------------------------------------------------
    */

/*    function _getResume($user_id)
    {

        if (!$user_id) return false;

        $user_resume = User_Resume::where("user_id", $user_id)->first();

        if (!$user_resume) return false;

        $result = [
            "resume" => [
                "sites" => $this->_get_resume_sites($user_resume->id),
                "description" => $user_resume->description,
                "attached_files_url" => $this->_get_resume_files($user_resume->id),
            ],
        ];

        return $result;
    }*/

    /*
    |--------------------------------------------------------------------------
    | Добавить/изменить резюме пользователю
    |--------------------------------------------------------------------------
    */

    /*function _saveResume($data)
    {
        $edit = false;
        $validation = $this->form_validation;

        $validation->set_data($data);
        $validation->set_rules($this->save_resume_rules);

        if( ! $validation->run()){
            $this->errors = $validation->error_array();
            return false;
        }

        $resume = User_resume::where("user_id", $data['user_id'])->first();

        if ($resume) $edit = true;

        $resume = User_resume::updateOrCreate(['user_id' => $data['user_id']]);
        $resume->user_id = $data['user_id'];
        $resume->description = $data['description'];

        if( $resume->save()){
            $attach_ids   = isset($data['attach_ids']) ? $data['attach_ids'] : [];
            $resume_sites = isset($data['sites']) ? $data['sites'] : [];


            //if ($edit)
            //    $this->_removeAttaches(PARENT_USER_RESUME, $user_resume->id);

            //$this->_addAttaches(PARENT_USER_RESUME, $user_resume->id, $attach_ids);
            //$this->add_cites($resume_sites, $user_id, $edit);

            return ['resume_id' => $resume->id];
        }else{
            return false;
        }

    }*/


    /*
    |--------------------------------------------------------------------------
    | Добавляем учасников к сущности
    |--------------------------------------------------------------------------
    */

    function _share($user_ids = [], $entity_type, $entity_id){

        foreach ($user_ids as $key => $user_id) {

            $user = User::find($user_id);
            if (!$user) {
                unset($user_ids[$key]);
                continue;
            }

            $user_count = Share::where([
                'entity_type' => $entity_type,
                'entity_id' => $entity_id,
                'user_id'   => $entity_id
            ])->count();
            if($user_count) {
                unset($user_ids[$key]);
                continue;
            }

            Share::insert([
                'entity_type' => $entity_type,
                'entity_id' => $entity_id,
                'user_id'   => $entity_id
            ]);
        }
        return $user_ids;
    }

    /*
    |--------------------------------------------------------------------------
    | Получить учасников сущности
    |--------------------------------------------------------------------------
    */

    function _getShareIds($entity_type, $entity_id){
        $shares = Share::where([
            'entity_type' => $entity_type,
            'entity_id' => intval($entity_id)
        ])->pluck('user_id')->toArray();

        return $shares;
    }

    /*
    |--------------------------------------------------------------------------
    | Удаляем учасников сущности
    |--------------------------------------------------------------------------
    */

    function _removeShares($entity_type, $entity_id){

        $shares = Share::where([
            'entity_type' => $entity_type,
            'entity_id' => $entity_id
        ]);

        $affected = $shares->delete();

        return $affected;
    }



}