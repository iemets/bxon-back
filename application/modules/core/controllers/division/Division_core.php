<?php
class Division_core extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

    }

    function index()
    {
        return $this->_getList();
    }

    /*
    |--------------------------------------------------------------------------
    | Получаем список подразделений
    |--------------------------------------------------------------------------
    */

    function _getList($filters = null, $sort_by = null, $sort_order = null, $limit = null, $offset = null)
    {

        if (!in_array($sort_by, ['name'])) {
            $sort_by = 'id';
        }

        if (!in_array(mb_strtolower($sort_order), ['asc', 'desc'])) {
            $sort_by = 'asc';
        }

        $limit = (int)$limit;

        if ($limit < 1 && $limit > 100) {
            $limit = 10;
        }

        $offset = (int)$offset;


        $divisions = new Division();

        if (isset($filters['id'])) {

        }

        return $divisions->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Проверка подразделения
    |--------------------------------------------------------------------------
    */

    function _validDivision($role, $division_id){

        if ($role == User::ROLE_DEVELOPER || $role == User::ROLE_LEADER) {
            if (!isset($division_id)) {
                $this->errors[] = array("field" => "division_id", "message" => "required");
                $this->send(422, $this->errors);
                return false;
            } else {
                $division = Division::find($division_id);
                if (!$division) {
                    $this->errors[] = array("field" => "division_id", "message" => "division_not_found");
                    $this->send(422, $this->errors);
                    return false;
                }
            }
        }

        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Сохранить подразделение пользователя
    |--------------------------------------------------------------------------
    */

    function _addUser($user_id, $division_id){
        $division = Division::find($division_id);
        if (!$division) {
            return false;
        }
        $user_division = User_Division::firstOrNew(['user_id' => $user_id]);
        $user_division->user_id = $user_id;
        $user_division->division_id = $division_id;
        $save = $user_division->save();
        if (!$save) {
            return false;
        }
        return true;
    }


}