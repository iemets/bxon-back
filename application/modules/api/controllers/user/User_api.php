<?php
use Illuminate\Database\Capsule\Manager as DB;

class User_api extends Api_Controller{

    function __construct(){
        parent::__construct();
    }

    function get($id = null){

        if($id){
            $user = $this->_getById($id);
            $this->output->send($user, 200);
        }else{
            $page       = intval(get('page'));
            $limit      = intval(get('limit'));
            $filters    = get('filters');
            $sort_by    = get('sort_by');
            $sort_order = get('sort_order');

            if( ! $page) $page = 1;

            $offset = $page * $limit - $limit;

            $users = $this->_getList($filters, $sort_by, $sort_order, $offset, $limit);

            $this->output->send($users, 200);
        }
    }

    function _getById($id){
        $user = User::where('id', $id);

        if( ! $user->count()){
            $this->output->send(null, 200);
        }

        return $user = $user->with('properties', 'categories', 'division')->first()->toArray();
    }

    function _getList($filters = null, $sort_by = null, $sort_order = null, $offset = null, $limit = 10){

        if( ! in_array($sort_by, ['id', 'role', 'email'])){
            $sort_by = 'id';
        }

        if( ! in_array(mb_strtolower($sort_order), ['asc', 'desc'])){
            $sort_order = 'asc';
        }

        $limit = (int) $limit;

        if($limit < 1 || $limit > 100){
            $limit = 10;
        }

        $offset = (int) $offset;

        $user = new User();

        if(isset($filters['role']) && in_array($filters['role'], $this->roles)){
            $user = $user->where(
                'role', '=', $filters['role']
            );
        }

        if( ! empty($filters['email'])){
            $user = $user->where(
                'email', 'like', $filters['email'].'%'
            );
        }

        $user = $user->with('categories', 'properties', 'division');

        if( ! empty($filters['category'])){
            $user = $user->whereHas('categories', function($query) use ($filters){
                $query->where([
                    'id' => $filters['category']
                ]);
            });
        }

        if( ! empty($filters['label'])){
            $user = $user->where(
                'label', 'like', $filters['label'].'%'
            );
        }

        $total = $user->count();

        $user = $user->skip($offset)->take($limit)->orderBy($sort_by, $sort_order)->get();

        if ( ! $user->count()) return null;

        $user['pagination'] = array(
            "limit" => $limit,
            "total" => $total
        );

        return $user->toArray();
    }

    function add(){
        $data = json();

        $validation = $this->_setValidation($data);

        if( ! $validation->run($this)){
            $this->output->send($validation->get_errors(), 422);
        }

        $data['user']['password'] = md5($data['user']['password']);

        $user = new User();
        $user->fill($data['user']);

        if( ! $user->save()) {
            $this->output->send(['user' => 'error.edit_user'], 500);
            return false;
        }

        $this->load->module('api/category/Category_api', null, 'category');

        if( ! empty($data['categories'])){
            foreach($data['categories'] as $category){
                $this->category->_bind(Category_Entity::ENTITY_TYPE_USER, $user->id, $category);
            }
        }

        $this->load->module('api/user/User_Property_api', null, 'property');
        $this->property->_add($user->id, $this->property->_convert($data, User_Property::PROPERTY_TYPES));

        $this->load->module('api/user/User_Division_api', null, 'division');
        //$this->division->_add($data['division']);

        $this->load->module('api/user/User_Resume_api', null, 'resume');
        $this->resume->_add($user->id, $data['resume']);

        $this->load->module('api/user/User_Shedule_api', null, 'shedule');
        $this->shedule->_add($user->id, $data['shedule']);

        $this->output->send(['user_id' => $user->id], 200);
    }

    function edit($user_id){

        if( ! $this->_exist($user_id)){
            $this->output->send(['errors' => 'error.user_exist']);
        }

        $data = json();

        $validation = $this->_setValidation($data);

        if( ! $validation->run($this)){
            $this->output->send($validation->get_errors(), 422);
        }

        $user = User::where('id', $user_id)->first();
        $user->guard(['password', 'email']);
        $user->fill($data['user']);

        if( ! $user->save()) {
            $this->output->send(['user' => 'error.edit_user'], 500);
            return false;
        }

        $this->load->module('api/category/Category_api', null, 'category');

        if( ! empty($data['categories'])){
            $this->category->_unbind(Category_Entity::ENTITY_TYPE_USER, $user->id);
            foreach($data['categories'] as $category_id){
                $this->category->_bind($category_id, Category_Entity::ENTITY_TYPE_USER, $user->id);
            }
        }

        $this->load->module('api/user/User_Property_api', null, 'property');
        $this->property->_add($user->id, $this->property->_convert($data, User_Property::PROPERTY_TYPES));

        $this->load->module('api/user/User_Division_api', null, 'division');
        $this->division->_add($user_id, $data['division']);

        $this->load->module('api/user/User_Resume_api', null, 'resume');
        $resume = $this->resume->_getById($user->id);

        if( ! $resume){
            $this->resume->_add($user->id, $data['resume']);
        }else{
            $this->resume->_edit($user->id, $data['resume']);
        }

        $this->load->module('api/user/User_Portfolio_api', null, 'portfolio');
        //$this->portfolio->_add($user_id, $data['portfolio']);

        $this->load->module('api/user/User_Shedule_api', null, 'shedule');
        $this->shedule->_remove($user->id);
        $this->shedule->_add($user->id, $data['shedule']);

        $this->output->send(['user_id' => $user->id], 200);
    }

    function remove($user_id){

        if( ! $this->_exist($user_id)){
            $this->output->send('User not exist', 422);
        }

        $user = User::where('id', $user_id);

        $categories = Category_Entity::where([
            'entity_type' => Category_Entity::ENTITY_TYPE_USER,
            'entity_id' => $user_id
        ]);

        $properties = User_Property::where([
            'user_id' => $user_id
        ]);

        $user->delete();

        $this->output->send([
            'user_id' => $user_id,
            'properties' => $properties->delete(),
            'categories' => $categories->delete(),
        ], 200);
    }

    function _exist($id){
        return (bool) User::where('id', $id)->count();
    }

    function _setValidation($data){

        $validation = $this->form_validation;
        $validation->set_data($data);

        $validation->set_rules('user[email]', 'email', 'trim|required|valid_email');
        $validation->set_rules('user[role]', 'role', 'trim|required|in_list['.implode(',', Role::ROLES).']');
        $validation->set_rules('user[first_name]', 'first_name', 'trim|required|max_length[255]');
        $validation->set_rules('user[middle_name]', 'middle_name', 'trim|required|max_length[255]');
        $validation->set_rules('user[last_name]', 'last_name', 'trim|required|max_length[255]');

        if(isset($data['user']['role']) && in_array($data['user']['role'], [Role::ROLE_UM])){
            $validation->set_rules('user[division_id]', 'division_id', 'trim|required|numeric|exist[User_Division]');
        }

        if(isset($data['email'])){
            if(is_string($data['email'])){
                $validation->set_rules('email', 'email', 'max_length[255]');
            }elseif(is_array($data['email'])){
                foreach($data['email'] as $key => $email){
                    $validation->set_rules('email['.$key.']', 'email['.$key.']', 'required|max_length[255]|valid_email');
                }
            }
        }

        if(isset($data['skype'])){
            if(is_string($data['skype'])){
                $validation->set_rules('skype', 'skype', 'max_length[255]');
            }elseif(is_array($data['skype'])){
                foreach($data['skype'] as $key => $email){
                    $validation->set_rules('skype['.$key.']', 'skype['.$key.']', 'required|max_length[255]');
                }
            }
        }

        if(isset($data['phone'])){
            if(is_string($data['phone'])) {
                $validation->set_rules('phone', 'phone', 'required|max_length[255]');
            }elseif(is_array($data['phone'])){
                $validation->set_rules('phone[0]', 'phone', 'required|max_length[255]');
                foreach($data['phone'] as $key => $phone){
                    $validation->set_rules('phone['.$key.']', 'phone ' . $key, 'required|max_length[255]');
                }
            }
        }

        if(isset($data['categories'])){
            if(is_string($data['categories'])) {
                $validation->set_rules('categories', 'categories', 'required|numeric|exist[Category]');
            }elseif(is_array($data['categories'])){
                foreach($data['categories'] as $key => $category){
                    $validation->set_rules('categories['.$key.']', 'categories['.$key.']', 'required|numeric|exist[Category]');
                }
            }
        }

        if(isset($data['resume'])){
            $validation->set_rules('resume[description]', 'categories', 'required');
            if(isset($data['resume']['sites'])){
                if(is_string($data['resume']['sites'])){
                    $validation->set_rules('resume[sites]', 'resume[sites]', 'required|max_length[1000]');
                }elseif(is_array($data['resume']['sites'])){
                    foreach ($data['resume']['sites'] as $key => $site) {
                        $validation->set_rules('resume[sites]['.$key.']', 'resume[sites]['.$key.']', 'required|max_length[1000]');
                    }
                }
            }else{
                $validation->set_rules('resume[sites]', 'resume[sites]', 'required');
            }
        }

        $validation->set_rules('shedule[monday][time_from]', 'monday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('shedule[monday][time_to]', 'monday', 'required|numeric|greater_than['.(isset($data['shedule']['monday']['time_from']) ? $data['shedule']['monday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('shedule[tuesday][time_from]', 'tuesday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('shedule[tuesday][time_to]', 'tuesday', 'required|numeric|greater_than['.(isset($data['shedule']['tuesday']['time_from']) ? $data['shedule']['tuesday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('shedule[wednesday][time_from]', 'wednesday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('shedule[wednesday][time_to]', 'wednesday', 'required|numeric|greater_than['.(isset($data['shedule']['wednesday']['time_from']) ? $data['shedule']['wednesday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('shedule[thursday][time_from]', 'thursday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('shedule[thursday][time_to]', 'thursday', 'required|numeric|greater_than['.(isset($data['shedule']['thursday']['time_from']) ? $data['shedule']['thursday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('shedule[friday][time_from]', 'friday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('shedule[friday][time_to]', 'friday', 'required|numeric|greater_than['.(isset($data['shedule']['friday']['time_from']) ? $data['shedule']['friday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('shedule[saturday][time_from]', 'saturday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('shedule[saturday][time_to]', 'saturday', 'required|numeric|greater_than['.(isset($data['shedule']['saturday']['time_from']) ? $data['shedule']['saturday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('shedule[sunday][time_from]', 'sunday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('shedule[sunday][time_to]', 'sunday', 'required|numeric|greater_than['.(isset($data['shedule']['sunday']['time_from']) ? $data['shedule']['sunday']['time_from'] : 0) .']|less_than[86400]');


        return $validation;
    }
}