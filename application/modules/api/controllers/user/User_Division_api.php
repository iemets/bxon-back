<?php
class User_Division_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
    }

    function add(){

    }

    function _add(){

    }

    function edit(){

    }

    function _edit(){

    }

    function remove(){

    }

    function _remove(){

    }

    function _exist($id){
        return (bool) User_Division::where('id', $id)->count();
    }

    function _bind($division_id, $user_id){
        $user_division = User_Division::where('id', $division_id);

        if( ! $user_division->count()) return false;

        $user_division = $user_division->first();
        $user_division->user_id = $user_id;
        return $user_division->save();
    }
}