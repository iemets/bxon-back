<?php

use Illuminate\Database\Capsule\Manager as DB;

class Project_Access_Group_api extends Api_Controller
{

    function __construct()
    {
        parent:: __construct();
        $this->load->module('api/file/File_Entity_api', null, 'file_entity');
        $this->load->module('api/project/Project_api', null, 'project');
        $this->load->module('api/project/Project_Access_api', null, 'project_access');
        $this->load->module('api/user/User_Entity_api', null, 'user_entity');

    }

    function index()
    {
    }

    /*
    |--------------------------------------------------------------------------
    | Установка валидации
    |--------------------------------------------------------------------------
    */

    function _setValidation($data)
    {
        $validation = $this->form_validation;
        $validation->set_data($data);

        $validation->set_rules('project_id', 'project_id', 'required|integer');
        $validation->set_rules('title', 'title', 'required|max_length[256]');

        if (isset($data['project_id'])) {
            $validation->set_rules('project_id', 'project_id', 'trim|required|numeric|user_func[api/project/Project_api/_projectExist]', ['user_func' => 'Project not exist']);
        }

        return $validation;
    }


    /*
    |--------------------------------------------------------------------------
    | Существует ли группа доступов
    |--------------------------------------------------------------------------
    */

    function _accessGroupExist($id){
        return (bool) Project_Access_Group::where('id', $id)->count();
    }

    /*
    |--------------------------------------------------------------------------
    | Добавить группу для доступов
    |--------------------------------------------------------------------------
    */

    function add()
    {
        $data = json();

        $validation = $this->_setValidation($data);

        if (!$validation->run($this)) {
            $this->output->send($validation->get_errors(), 422);
        }

        $access_group = new Project_Access_Group();

        $access_group->fillable([
            "project_id",
            "title"
        ]);

        $access_group->fill($data);

        if (!$access_group->save()) {
            $this->output->send(['access_group' => 'error.project_access_group.update'], 500);
            return false;
        }

        $user_ids  = isset($data['user_ids']) ? $data['user_ids'] : [];
        $res = $this->user_entity->_bind(Project_Access_Group::ENTITY_PROJECT_ACCESS_GROUP, $access_group->id, $user_ids);

        $this->output->send(['access_group_id' => $access_group->id], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Редактировать группу доступов
    |--------------------------------------------------------------------------
    */

    function edit($project_id, $access_group_id){
        if( ! $this->_accessGroupExist($access_group_id)){
            $this->output->send(['errors' => 'error.project_access_group.not_exist']);
        }

        $data = json();

        $validation = $this->_setValidation($data);

        if (!$validation->run($this)) {
            $this->output->send($validation->get_errors(), 422);
        }

        $access_group = Project_Access_Group::where('id', $access_group_id)->first();

        $access_group->fillable([
            "project_id",
            "title"
        ]);

        $access_group->fill($data);

        if (!$access_group->save()) {
            $this->output->send(['access' => 'error.project_access_group.update'], 500);
            return false;
        }

        $user_ids  = isset($data['user_ids']) ? $data['user_ids'] : [];
        $this->user_entity->_unbind(Project_Access_Group::ENTITY_PROJECT_ACCESS_GROUP, $access_group->id);
        $this->user_entity->_bind(Project_Access_Group::ENTITY_PROJECT_ACCESS_GROUP, $access_group->id, $user_ids);

        $this->output->send(['access_id' => $access_group->id], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Удалить группу доступов
    |--------------------------------------------------------------------------
    */

    function remove($project_id, $access_group_id)
    {

        $access_group = Project_Access_Group::where('id', $access_group_id);

        if (!$access_group){
            $this->output->send(['errors' => 'error.project_access_group.not_exist']);
            return false;
        }

        DB::beginTransaction();

        $access_ids = Project_Access::where('access_group_id', $access_group_id)->pluck('id')->toArray();

        foreach($access_ids as $access_id){
            $this->project_access->_remove($project_id, $access_id);
        }

        $this->user_entity->_unbind(Project_Access_Group::ENTITY_PROJECT_ACCESS_GROUP, $access_group_id);


        if ($access_group->delete()) {
            DB::commit();
            $result = true;
        } else {
            DB::rollback();
            $result = false;
        }

        if ($result) {
            $this->output->send(['access_group_id' => $access_group_id], 200);
        } else {
            $this->output->send(['errors' => ['delete_project_access_group' => 'errors.project_access_group.delete']], 500);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Получить группу доступов
    |--------------------------------------------------------------------------
    */

    function get($project_id, $access_group_id){
        $access_group = Project_Access_Group::where('id', $access_group_id);

        if( ! $access_group->count()){
            $this->output->send(null, 200);
        }

        $access_group = $access_group->with('users')->first()->toArray();

        $this->output->send($access_group, 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Получить список групп доступов
    |--------------------------------------------------------------------------
    */

    function getList(){
        $page = intval(get('page'));
        $limit = intval(get('limit'));
        $filters = get('filters');
        $sort_by = get('sort_by');
        $sort_order = get('sort_order');

        if (!$page) $page = 1;

        $offset = $page * $limit - $limit;

        $access_groups = $this->_getList($filters, $sort_by, $sort_order, $offset, $limit);

        $this->output->send($access_groups, 200);
    }

    function _getList($filters = null, $sort_by = null, $sort_order = null, $offset = null, $limit = 10)
    {

        if (!in_array($sort_by, ['id', 'project_id'])) {
            $sort_by = 'id';
        }

        if (!in_array(mb_strtolower($sort_order), ['asc', 'desc'])) {
            $sort_order = 'asc';
        }

        $limit = (int)$limit;

        if ($limit < 1 || $limit > 100) {
            $limit = 10;
        }

        $offset = (int)$offset;

        $access_groups = new Project_Access_Group();

        $access_groups = $access_groups->with('users');

        if (isset($filters['project_id'])){
            $access_groups = $access_groups->where('project_id', (int)$filters['project_id']);
        }

        $total = $access_groups->count();
        $access_groups = $access_groups->skip($offset)->take($limit)->orderBy($sort_by, $sort_order)->get();

        $count = $access_groups->count();

        if (!$count) return null;

        $access_groups['pagination'] = array(
            "limit" => $limit,
            "total" => $total
        );

        return $access_groups->toArray();
    }
}
