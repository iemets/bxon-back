<?php
class User_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
    }

    function get($id = null){

        if($id){
            $filters = ['id' => $id];
            $user = $this->_get($filters, null, null, null, null, ['categories', 'properties', 'division', 'schedule', 'payments', 'wallets', 'tags']);
            if( ! $user){
                $this->output->send('User not found', 422);
            }

            $this->output->send($user->first(), 200);
        }else{
            $page       = intval(get('page'));
            $limit      = intval(get('limit'));
            $filters    = get('filters');
            $sort_by    = get('sort_by');
            $sort_order = get('sort_order');

            if( ! $page) $page = 1;

            $offset = $page * $limit - $limit;

            $users = $this->_get($filters, $sort_by, $sort_order, $offset, $limit);
            $total = $users->count();
            $users = $users->toArray();

            $this->output->send([
                'users' => $users,
                "offset" => $offset,
                "total" => $total
            ], 200);
        }
    }

    function _get($filters = null, $sort_by = null, $sort_order = null, $offset = null, $limit = 10, $with = null){

        if( ! in_array($sort_by, ['id', 'role', 'email'])){
            $sort_by = 'id';
        }

        if( ! in_array(mb_strtolower($sort_order), ['asc', 'desc'])){
            $sort_order = 'asc';
        }

        $limit = (int) $limit;

        if($limit < 1 || $limit > 100){
            $limit = 10;
        }

        $offset = (int) $offset;

        $user = new User();

        if(isset($filters['id'])){
            $user = $user->where(
                'id', '=', $filters['id']
            );
        }

        if(isset($filters['role']) && in_array($filters['role'], $this->roles)){
            $user = $user->where(
                'role', '=', $filters['role']
            );
        }

        if( ! empty($filters['email'])){
            $user = $user->where(
                'email', 'like', $filters['email'].'%'
            );
        }

        $user = $user->with( (array) $with);

        if( ! empty($filters['category'])){
            $user = $user->whereHas('categories', function($query) use ($filters){
                $query->where([
                    'id' => $filters['category']
                ]);
            });
        }

        if( ! empty($filters['label'])){
            $user = $user->where(
                'label', 'like', $filters['label'].'%'
            );
        }

        $user = $user->skip($offset)->take($limit)->orderBy($sort_by, $sort_order)->get();

        if ( ! $user->count()) return null;

        return $user;
    }

    function add(){
        $data = json();

        $validation = $this->_setValidation($data);

        if( ! $validation->run($this)){
            $this->output->send($validation->get_errors(), 422);
        }

        $password = random_string();
        $data['user']['password'] = md5($password);

        $user_id = $this->_save($data);

        $this->output->send($user_id, 200);
    }

    function edit($user_id){
        if( ! $this->_exist($user_id)){
            $this->output->send('User not found', 422);
        }

        $data = json();

        $validation = $this->_setValidation($data);

        if( ! $validation->run($this)){
            $this->output->send($validation->get_errors(), 422);
        }

        $user_id = $this->_save($data, $user_id);

        $this->output->send($user_id, 200);
    }

    function remove($user_id){
        $user = User::find($user_id);

        if( ! $user)
            $this->output->send('User not exist', 422);

        $user->delete();

        $this->output->send(['user_id' => $user->id], 200);
    }

    function _setValidation($data){

        $validation = $this->form_validation;
        $validation->set_data($data);

        $validation->set_rules('user[email]', 'email', 'trim|required|valid_email');
        $validation->set_rules('user[role]', 'role', 'trim|required|in_list['.implode(',', Role::ROLES).']');
        $validation->set_rules('user[first_name]', 'first_name', 'trim|required|max_length[255]');
        $validation->set_rules('user[middle_name]', 'middle_name', 'trim|required|max_length[255]');
        $validation->set_rules('user[last_name]', 'last_name', 'trim|required|max_length[255]');

        if(isset($data['user']['role']) && in_array($data['user']['role'], [Role::ROLE_UM])){
            $validation->set_rules('user[division_id]', 'division_id', 'trim|required|numeric|exist[Division]');
        }

        if(isset($data['properties']['email'])){
            if(is_string($data['properties']['email'])){
                $validation->set_rules('properties[email]', 'email', 'max_length[255]');
            }elseif(is_array($data['properties']['email'])){
                foreach($data['properties']['email'] as $key => $email){
                    $validation->set_rules('properties[email]['.$key.']', 'email['.$key.']', 'required|max_length[255]|valid_email');
                }
            }
        }

        if(isset($data['properties']['skype'])){
            if(is_string($data['properties']['skype'])){
                $validation->set_rules('properties[skype]', 'skype', 'max_length[255]');
            }elseif(is_array($data['properties']['skype'])){
                foreach($data['properties']['skype'] as $key => $email){
                    $validation->set_rules('properties[skype]['.$key.']', 'skype['.$key.']', 'required|max_length[255]');
                }
            }
        }

        if(isset($data['properties']['phone'])){
            if(is_string($data['properties']['phone'])) {
                $validation->set_rules('properties[phone]', 'phone', 'required|max_length[255]');
            }elseif(is_array($data['properties']['phone'])){
                $validation->set_rules('properties[phone][0]', 'phone', 'required|max_length[255]');
                foreach($data['properties']['phone'] as $key => $phone){
                    $validation->set_rules('properties[phone]['.$key.']', 'phone' . $key, 'required|max_length[255]');
                }
            }
        }

        if(isset($data['categories'])){
            if(is_string($data['categories'])) {
                $validation->set_rules('categories', 'categories', 'required|numeric|exist[Category]');
            }elseif(is_array($data['categories'])){
                foreach($data['categories'] as $key => $category){
                    $validation->set_rules('categories['.$key.']', 'categories['.$key.']', 'required|numeric|exist[Category]');
                }
            }
        }

        if(isset($data['resume'])){
            $validation->set_rules('resume[description]', 'categories', 'required');
            if(isset($data['resume']['sites'])){
                if(is_string($data['resume']['sites'])){
                    $validation->set_rules('resume[sites]', 'resume[sites]', 'required|max_length[1000]');
                }elseif(is_array($data['resume']['sites'])){
                    foreach ($data['resume']['sites'] as $key => $site) {
                        $validation->set_rules('resume[sites]['.$key.']', 'resume[sites]['.$key.']', 'required|max_length[1000]');
                    }
                }
            }else{
                $validation->set_rules('resume[sites]', 'resume[sites]', 'required');
            }
        }
        
        if(isset($data['portfolio'])){
            $validation->set_rules('portfolio[description]', 'categories', 'required');
            if(isset($data['portfolio']['sites'])){
                if(is_string($data['portfolio']['sites'])){
                    $validation->set_rules('portfolio[sites]', 'portfolio[sites]', 'required|max_length[1000]');
                }elseif(is_array($data['portfolio']['sites'])){
                    foreach ($data['portfolio']['sites'] as $key => $site) {
                        $validation->set_rules('portfolio[sites]['.$key.']', 'portfolio[sites]['.$key.']', 'required|max_length[1000]');
                    }
                }
            }else{
                $validation->set_rules('portfolio[sites]', 'portfolio[sites]', 'required');
            }
        }

        if(isset($data['tags'])){
            if(is_array($data['tags'])){
                foreach ($data['tags'] as $key => $site) {
                    $validation->set_rules('tags['.$key.']', 'tags['.$key.']', 'required|numeric|exist[Tag]');
                }
            }else{
                $validation->set_rules('tags', 'tags', 'numeric|exist[Tag]');
            }
        }

        if(isset($data['bank'])){
            $validation->set_rules('bank[name]', 'bank[]', 'trim|max_length[255]');
            $validation->set_rules('bank[BIK]', 'bank[BIK]', 'trim|max_length[255]');
            $validation->set_rules('bank[account]', 'bank[account]', 'trim|max_length[255]');
            $validation->set_rules('bank[correspondent_account]', 'bank[correspondent_account]', 'trim|max_length[255]');
        }

        $validation->set_rules('ownership[type]', 'ownership[type]', 'trim|required|in_list['.implode(',', Ownership::TYPES).']');

        if(isset($data['ownership']['files'])){

            if(isset($data['ownership']['files'])){
                if(is_string($data['ownership']['files'])){
                    $validation->set_rules('ownership[files]', 'ownership[files]', 'required|numeric|exist[File]');
                }elseif(is_array($data['ownership']['files'])){
                    foreach ($data['ownership']['files'] as $key => $site) {
                        $validation->set_rules('ownership[files]['.$key.']', 'portfolio[sites]['.$key.']', 'required|numeric|exist[File]');
                    }
                }
            }

            if($data['ownership']['type'] === Ownership::TYPE_PERSON){
                $validation->set_rules('ownership[person][first_name]', 'ownership[person][first_name]', 'trim|max_length[255]');
                $validation->set_rules('ownership[person][middle_name]', 'ownership[person][middle_name]', 'trim|max_length[255]');
                $validation->set_rules('ownership[person][last_name]', 'ownership[person][last_name]', 'trim|max_length[255]');
                $validation->set_rules('ownership[person][location]', 'ownership[person][location]', 'trim|max_length[255]');
                $validation->set_rules('ownership[person][address]', 'ownership[person][address]', 'trim|max_length[255]');
                $validation->set_rules('ownership[person][INN]', 'ownership[person][INN]', 'trim|max_length[255]');
                $validation->set_rules('ownership[person][site]', 'ownership[person][site]', 'trim|max_length[255]');
            }elseif($data['ownership']['type'] === Ownership::TYPE_SP){
                $validation->set_rules('ownership[sp][full_name]', 'ownership[sp][full_name]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][OGRN]', 'ownership[sp][OGRN]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][number]', 'ownership[sp][number]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][date]', 'ownership[sp][date]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][postal_address]', 'ownership[sp][postal_address]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][physical_address]', 'ownership[sp][physical_address]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][OKATO]', 'ownership[sp][OKATO]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][OPF]', 'ownership[sp][OPF]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][OKVED]', 'ownership[sp][OKVED]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][phone]', 'ownership[sp][phone]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][email]', 'ownership[sp][email]', 'trim|max_length[255]');
                $validation->set_rules('ownership[sp][site]', 'ownership[sp][site]', 'trim|max_length[255]');
            }elseif($data['ownership']['type'] === Ownership::TYPE_ORGANIZATION){
                $validation->set_rules('ownership[organization][full_name]', 'ownership[organization][full_name]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][short_name]', 'ownership[organization][short_name]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][OGRN]', 'ownership[organization][OGRN]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][legal_address]', 'ownership[organization][legal_address]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][postal_address]', 'ownership[organization][postal_address]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][physical_address]', 'ownership[organization][physical_address]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][OKATO]', 'ownership[organization][OKATO]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][director_full_name]', 'ownership[organization][director_full_name]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][accountant_full_name]', 'ownership[organization][accountant_full_name]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][phone]', 'ownership[organization][phone]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][email]', 'ownership[organization][email]', 'trim|max_length[255]');
                $validation->set_rules('ownership[organization][site]', 'ownership[organization][site]', 'trim|max_length[255]');
            }

            if($data['ownership']['type'] === Ownership::TYPE_ORGANIZATION || $data['ownership']['type'] === Ownership::TYPE_SP){
                $validation->set_rules('ownership[responsible][first_name]', 'ownership[responsible][first_name]', 'trim|required|in_list['.implode().']');
                $validation->set_rules('ownership[responsible][middle_name]', 'ownership[responsible][middle_name]', 'trim|required|in_list['.implode().']');
                $validation->set_rules('ownership[responsible][last_name]', 'ownership[responsible][last_name]', 'trim|required|in_list['.implode().']');
                $validation->set_rules('ownership[responsible][location]', 'ownership[responsible][location]', 'trim|required|in_list['.implode().']');
                $validation->set_rules('ownership[responsible][address]', 'ownership[responsible][address]', 'trim|required|in_list['.implode().']');
                $validation->set_rules('ownership[responsible][INN]', 'ownership[responsible][INN]', 'trim|required|in_list['.implode().']');
                $validation->set_rules('ownership[responsible][site]', 'ownership[responsible][site]', 'trim|required|in_list['.implode().']');
            }
        }

        $validation->set_rules('payment[description]', 'payment[description]', 'required|max_length[5000]');
        $validation->set_rules('payment[rate]', 'payment[rate]', 'required|numeric');
        $validation->set_rules('payment[currency]', 'payment[currency]', 'required|in_list['.implode(',', Payment::CURRENCIES).']');
        $validation->set_rules('payment[type]', 'payment[type]', 'required|in_list['.implode(',', Payment::TYPES).']');


        $validation->set_rules('schedule[monday][time_from]', 'monday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('schedule[monday][time_to]', 'monday', 'required|numeric|greater_than['.(isset($data['shedule']['monday']['time_from']) ? $data['shedule']['monday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('schedule[tuesday][time_from]', 'tuesday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('schedule[tuesday][time_to]', 'tuesday', 'required|numeric|greater_than['.(isset($data['shedule']['tuesday']['time_from']) ? $data['shedule']['tuesday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('schedule[wednesday][time_from]', 'wednesday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('schedule[wednesday][time_to]', 'wednesday', 'required|numeric|greater_than['.(isset($data['shedule']['wednesday']['time_from']) ? $data['shedule']['wednesday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('schedule[thursday][time_from]', 'thursday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('schedule[thursday][time_to]', 'thursday', 'required|numeric|greater_than['.(isset($data['shedule']['thursday']['time_from']) ? $data['shedule']['thursday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('schedule[friday][time_from]', 'friday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('schedule[friday][time_to]', 'friday', 'required|numeric|greater_than['.(isset($data['shedule']['friday']['time_from']) ? $data['shedule']['friday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('schedule[saturday][time_from]', 'saturday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('schedule[saturday][time_to]', 'saturday', 'required|numeric|greater_than['.(isset($data['shedule']['saturday']['time_from']) ? $data['shedule']['saturday']['time_from'] : 0) .']|less_than[86400]');
        $validation->set_rules('schedule[sunday][time_from]', 'sunday', 'required|numeric|greater_than[0]|less_than[86400]');
        $validation->set_rules('schedule[sunday][time_to]', 'sunday', 'required|numeric|greater_than['.(isset($data['shedule']['sunday']['time_from']) ? $data['shedule']['sunday']['time_from'] : 0) .']|less_than[86400]');

        return $validation;
    }

    function _save($data, $user_id = null){

        $user = new User();

        if($user_id){
            $user = $user->find($user_id);
            if( ! $user)
                $this->output->send('User not exist', 422);
        }

        $user->fill($data['user']);
        $user->save();

        $user_resume = Resume::firstOrNew(['user_id' => $user->id]);
        $user_resume->user_id = $user->id;
        $this->load->module('api/Resume_api', null, 'resume');
        $this->resume->_save($data['resume'], $user_resume);

        $user_portfolio = Portfolio::firstOrNew(['user_id' => $user->id]);
        $user_portfolio->user_id = $user->id;
        $this->load->module('api/Portfolio_api', null, 'portfolio');
        $this->portfolio->_save($data['portfolio'], $user_portfolio);

        $user_properties = $user->properties();
        $user_properties->delete();

        $properties = [];
        foreach($data['properties'] as $key => $property){
            foreach( (array) $property as $value){
                $properties[] = new Property([
                    'type' => $key,
                    'value' => $value
                ]);
            }
        }
        $user_properties->saveMany($properties);

        $user_categories = $user->categories();
        $user_categories->detach();
        $user_categories->attach($data['categories']);

        $user_payment = Payment::firstOrNew(['user_id' => $user->id]);
        $user_payment->user_id = $user->id;
        $user_payment->fill($data['payment']);
        $user_payment->save();

        $user_wallets = $user->wallets();
        $user_wallets->delete();
        $wallets = [];
        foreach($data['wallets'] as $key => $wallet){
            foreach( (array) $wallet as $value){
                $wallets[] = new Wallet([
                    'type' => $key,
                    'value' => $value
                ]);
            }
        }
        $user_wallets->saveMany($wallets);

        $user_tags = $user->tags();
        $user_tags->detach();
        $user_tags->attach($data['tags']);

        $user_schedule = $user->schedule();
        $user_schedule->delete();
        $schedules = [];
        foreach ($data['schedule'] as $key => $schedule) {
            $schedule['week_day'] = $key;
            $schedules[] = new Schedule($schedule);
        }
        $user_schedule->saveMany($schedules);

        if( ! isset($data['bank']))
            $data['bank'] = [];

        $user_bank = Bank::firstOrNew(['user_id' => $user->id]);
        $user_bank->fill($data['bank']);
        $user_bank->save();

        $user_ownership = Ownership::firstOrNew(['user_id' => $user->id]);
        $user_ownership->user_id = $user->id;
        $user_ownership->type = $data['ownership']['type'];

        $this->load->module('api/Ownership_api', null, 'ownership');
        $this->ownership->_save($data['ownership'], $user_ownership);

        return $user->id;
    }

    function _exist($id){
        return (bool) User::where('id', $id)->count();
    }

}