<?php
class Property_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
    }

    function _add($user_id, $property){

        $this->_remove($user_id, Property::PROPERTY_EMAIL);

        $data = [];
        $now = DateTime::createFromFormat('U', time());

        if( ! isset($property[0])){
            $properties = [$property];
        }else{
            $properties = $property;
        }

        foreach($properties as $property){
            $data[] = [
                'type' => $property['type'],
                'value' => $property['value'],
                'created_at' => $now->format('Y-m-d H:i:s'),
                'updated_at' => $now->format('Y-m-d H:i:s')
            ];
        }

        if( ! Property::insert($data)){
            return false;
        }

        return false;
    }

    function _remove($user_id, $type = null){
        $user_property = Property::where('user_id', $user_id);

        if($type && in_array($type, [Property::PROPERTY_TYPES])){
            $user_property = $user_property->where('type', $type);
        }

        return (bool) $user_property->delete();
    }

    function _convert($data, $list){
        $properties = [];

        foreach($list as $name){
            if( ! isset($data[$name])) continue;

            foreach( (array) $data[$name] as $prop){
                $properties[] = [
                    'type' => $name,
                    'value' => $prop
                ];
            }
        }
        return $properties;
    }

}