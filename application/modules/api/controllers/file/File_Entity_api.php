<?php
use Illuminate\Database\Capsule\Manager as DB;

class File_Entity_api extends Api_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->module('api/file/File_api', null, 'file');
    }

    /*
    |--------------------------------------------------------------------------
    | Привязка файла к сущности
    |--------------------------------------------------------------------------
    */

    function _bind($entity_type, $entity_id, $file_ids = []){

        foreach($file_ids as $key => $file_id){
            if( ! $this->file->_fileExist($file_id)) {
                unset($file_ids[$key]);
                continue;
            }

            if( ! in_array($entity_type, File_Entity::ENTITY_TYPES)){
                unset($file_ids[$key]);
                continue;
            }

            $file_entity = new File_Entity();
            $file_entity->file_id = $file_id;
            $file_entity->entity_type = ucfirst($entity_type);
            $file_entity->entity_id = $entity_id;

            $file_entity->save();
        }

        return $file_ids;
    }

    /*
    |--------------------------------------------------------------------------
    | Отвязываем файл от сущности, но не удаляем его (полное удаление по cron)
    |--------------------------------------------------------------------------
    */

    function _unbind($entity_type, $entity_id){
        $delete = File_Entity::where([
            'entity_id' => $entity_id,
            'entity_type' => $entity_type
        ])->delete();
        return $delete;
    }

}