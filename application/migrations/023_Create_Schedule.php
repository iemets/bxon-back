<?php
class Migration_Create_Schedule extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'week_day' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'time_from' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'time_to' => array(
                'type' => 'INT',
                'constraint' => 11,
            )
        ));
        //$this->dbforge->drop_table('schedule', true);
        $this->dbforge->create_table('schedule', true);

        $data = [
            [
                'user_id' => 1,
                'week_day' => 'monday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 1,
                'week_day' => 'tuesday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 1,
                'week_day' => 'wednesday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 1,
                'week_day' => 'thursday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 1,
                'week_day' => 'friday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 1,
                'week_day' => 'saturday',
                'time_from' => 28800,
                'time_to' => 64800
            ],

            [
                'user_id' => 2,
                'week_day' => 'sunday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 2,
                'week_day' => 'monday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 2,
                'week_day' => 'tuesday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 2,
                'week_day' => 'wednesday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 2,
                'week_day' => 'thursday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 2,
                'week_day' => 'friday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 2,
                'week_day' => 'saturday',
                'time_from' => 28800,
                'time_to' => 64800
            ],
            [
                'user_id' => 2,
                'week_day' => 'sunday',
                'time_from' => 28800,
                'time_to' => 64800
            ]
        ];

        foreach($data as $item){
            $schedule = new Schedule();
            $schedule->fill($item);
            $schedule->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('schedule', true);
    }
}