<?php

class Migration_Create_Portfolio extends CI_Migration {

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'description' => array(
                'type'       => 'VARCHAR',
                'constraint' => 128
            ),
            'created_at' => array(
                "type" => "datetime"
            ),
            'updated_at' => array(
                "type" => "datetime"
            ),
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('portfolio', true);
        $this->dbforge->create_table('portfolio', true);
    }

    function down(){
        $this->dbforge->drop_table('portfolio', true);
    }

}
