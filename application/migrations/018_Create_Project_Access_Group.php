<?php


class Migration_Create_Project_Access_Group extends CI_Migration
{

    function up()
    {

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),

            'project_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            )
        ));


        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('project_access_group', true);
        $this->dbforge->create_table('project_access_group', true);

        $data = [
            [
                'project_id' => 1,
                'title' => 'Тестовые доступы'
            ],
            [
                'project_id' => 1,
                'title' => 'Рабочие доступы'
            ],

        ];

        foreach ($data as $item) {
            $access_group = new Project_Access_Group();
            $access_group->fill($item);
            $access_group->save();
        }

	}

    function down(){
        $this->dbforge->drop_table('project_access_group', true);
    }
	
}