<?php

class Migration_Create_Task extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'project_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'created_at' => [
                "type" => "datetime"
            ],
            'updated_at' => [
                "type" => "datetime"
            ]
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('task', true);
        $this->dbforge->create_table('task', true);

        $data = [
            [
                'name' => 'задача 1',
                'project_id' => 1,
            ],
            [
                'name' => 'задача 2',
                'project_id' => 1,
            ],
            [
                'name' => 'задача 3',
                'project_id' => 2,
            ]
        ];

        foreach($data as $item){
            $user = new Task();
            $user->fill($item);
            $user->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('task', true);
    }

}
