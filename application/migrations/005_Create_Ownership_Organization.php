<?php
class Migration_Create_Ownership_Organization extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'ownership_id' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'full_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'short_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'OGRN' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'legal_address' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'postal_address' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'physical_address' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'OKATO' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'OPF' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'OKVED' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'director_full_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'accountant_full_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'phone' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'site' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ]
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->drop_table('ownership_organization', true);
        $this->dbforge->create_table('ownership_organization', true);
    }

    function down(){
        $this->dbforge->drop_table('ownership_organization', true);
    }
}