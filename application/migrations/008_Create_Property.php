<?php
class Migration_Create_Property extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'hash' => array(
                'type'       => 'varchar',
                'constraint' => 8,
                'comment'    => 'хэш данных',
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'comment'    => 'id сущности',
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'comment'    => 'тип свойства',
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'comment'    => 'значение',
            ),
            'created_at' => array(
                'type' => 'DATETIME',
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
            )
        ));

        $this->dbforge->add_key('id', true);
        $this->dbforge->drop_table('property', true);
        $this->dbforge->create_table('property', true);

        $data = [
            [
                'type' => 'skype',
                'value' => 'skype_1',
                'user_id' => 1
            ],
            [
                'type' => 'skype',
                'value' => 'skype_2',
                'user_id' => 1
            ],
            [
                'type' => 'email',
                'value' => 'email_1',
                'user_id' => 1
            ],
            [
                'type' => 'skype',
                'value' => 'skype_1',
                'user_id' => 2
            ],
            [
                'type' => 'email',
                'value' => 'email_1',
                'user_id' => 2
            ],
            [
                'type' => 'phone',
                'value' => 'phone_1',
                'user_id' => 3
            ]
        ];

        foreach($data as $item){
            $user = new Property();
            $user->fill($item);
            $user->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('property', true);
    }
}