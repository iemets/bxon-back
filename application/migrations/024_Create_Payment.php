<?php

class Migration_Create_Payment extends CI_Migration {

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'rate' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 11
            ),
            'first_payment' => array(
                'type' => 'DATETIME',
            ),
            'second_payment' => array(
                'type' => 'DATETIME',
            ),
            'currency' => array(
                'type' => 'VARCHAR',
                'constraint' => 10
            ),
            'description' => array(
                'type'       => 'VARCHAR',
                'constraint' => 128
            ),
            'created_at' => array(
                "type" => "datetime"
            ),
            'updated_at' => array(
                "type" => "datetime"
            )
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('payment', true);
        $this->dbforge->create_table('payment', true);
    }

    function down(){
        $this->dbforge->drop_table('payment', true);
    }

}
