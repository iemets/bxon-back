<?php

class Migration_Create_Security extends CI_Migration {

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'is_sms' => array(
                'type'       => 'INT',
                'constraint' => 1,
                'comment' => 'Запросить sms при первом входе'
            ),
            'is_additional' => array(
                'type'       => 'INT',
                'constraint' => 1,
                'comment' => 'Попросить дополнить данных при следущем входе'
            ),
            'is_email_confirm' => array(
                'type'       => 'INT',
                'constraint' => 1,
                'comment' => 'Запросить email потверждение'
            ),
            'created_at' => array(
                "type" => "datetime"
            ),
            'updated_at' => array(
                "type" => "datetime"
            ),
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('security', true);
        $this->dbforge->create_table('security', true);
    }

    function down(){
        $this->dbforge->drop_table('security', true);
    }

}
