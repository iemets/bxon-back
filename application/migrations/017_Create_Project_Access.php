<?php

class Migration_Create_Project_Access extends CI_Migration
{

    function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'project_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'access_group_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'inspection' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'comment' => '(manual, auto)',
            ),
            'protocol' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'comment' => 'тип аккаунта (FTP, SSH, Admin panel, misc)'
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => 50
            ),
            'server' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'comment' => 'сервер',
            ),
            'login' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'comment' => 'логин пользователя',
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => 100
            ),
            'comment' => array(
                'type' => 'VARCHAR',
                'constraint' => 300,
                'comment' => 'примечание',
            ),
            'is_correct' => array(
                'type' => 'INT',
                'constraint' => 1,
                'comment' => 'корректен ли доступ'
            ),
            'is_checked' => array(
                'type' => 'INT',
                'constraint' => 1,
                'comment' => 'проверен ли доступ'
            ),
            'created_at' => [
                "type" => "DATETIME"
            ],
            'updated_at' => [
                "type" => "DATETIME"
            ],
            'checked_at' => array(
                'type' => 'DATETIME',
                'comment' => 'Дата и время последней проверки доступов'
            )
        ));

        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('project_access', true);
        $this->dbforge->create_table('project_access', true);


        $data = [
            [
                'project_id' => 1,
                'access_group_id' => 1,
                'inspection' => 'manual',
                'protocol' => 'FTP',
                'title' => 'Доступы к серверу BXON_13',
                'server' => 'bxon13.hosting.ua',
                'login' => 'admin',
                'password' => 'qwerty',
                'comment' => 'Доступы только с айпи 156.99.52.147'

            ],
            [
                'project_id' => 1,
                'access_group_id' => 2,
                'inspection' => 'auto',
                'protocol' => 'FTP',
                'title' => 'Доступы к серверу BXON_14',
                'server' => 'bxon14.hosting.ua',
                'login' => 'master',
                'password' => '5445544',
                'comment' => 'Доступы только с айпи 156.99.52.147'

            ],

        ];

        foreach ($data as $item) {
            $access = new Project_Access();
            $access->fill($item);
            $access->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('project_access', true);
    }

}