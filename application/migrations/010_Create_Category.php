<?php
class Migration_Create_Category extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'parent_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'comment'    => 'ID рододителя',
            ),
            'sort_id' => array(
                'type' => 'INT',
                'constraint' => 3
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 128,
                'comment'    => 'Название',
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'comment'    => 'Описание',
            )
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('category', true);
        $this->dbforge->create_table('category', true);

        $data = [
            [
                'name' => 'Категория 1',
                'parent_id' => 0
            ],
            [
                'name' => 'Категория 2',
                'parent_id' => 1
            ],
            [
                'name' => 'Категория 3',
                'parent_id' => 2
            ],
            [
                'name' => 'Категория 4',
                'parent_id' => 0
            ],
            [
                'name' => 'Категория 5',
                'parent_id' => 4
            ]
        ];

        foreach($data as $item){
            $category = new Category();
            $category->fill($item);
            $category->save();
        }

    }

    function down(){
        $this->dbforge->drop_table('category', true);
    }
}